import { MobileElementCommand } from "./moladin.element.commands";
export class MoladinExpectedConditionCommand {
    _elementCommand: MobileElementCommand;

    constructor() {
        this._elementCommand = new MobileElementCommand();
    }
    
    isElementDisplayed = async (selector:string, strategy:string='CSS || XPATH'): Promise<boolean> => {
        const element = await this._elementCommand.finder(selector, strategy);
        const displayed = await element.isDisplayed();
        return displayed;
    }

    isElementNotDisplayed = async (selector:string, strategy:string='CSS || XPATH'): Promise<boolean> => {
        const element = await this._elementCommand.finder(selector, strategy);
        const displayed = await element.isDisplayed();
        return !(displayed);
    }

    waitUntilElementDisplayed = async (selector:string, timeoutDuration:number = 10000, strategy:string = 'CSS || XPATH'): Promise<void> => {
        const element = await this._elementCommand.finder(selector, strategy);
        await element.waitUntil(async () => (await this.isElementDisplayed(selector, strategy)) === true, { timeout: timeoutDuration, timeoutMsg: 'Element Not Found' });
    }

    waitUntilElementNotDisplayed = async (selector:string, timeoutDuration:number = 10000, strategy:string = 'CSS || XPATH'): Promise<void> => {
        const element = await this._elementCommand.finder(selector, strategy);
        await element.waitUntil(async () => (await this.isElementNotDisplayed(selector, strategy)) === true, { timeout: timeoutDuration, timeoutMsg: 'Element Still Found' });
    }
}