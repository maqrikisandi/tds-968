/* global $, $$ */
export class MobileElementCommand {
    defaultSelector: string;
    selector: string;

    constructor() {
        this.defaultSelector = '';
        this.selector = '';
    }

    finder = async (selector:string, strategy:string = 'CSS || XPATH'): Promise<any> => {
        let prefixSelector = this.defaultSelector;
        
        if(strategy === 'UISelector') {
            prefixSelector = 'android=';
        }
        return $(`${prefixSelector}${selector}`);
    }

    finders = async (selector:string, strategy:string = 'CSS || XPATH'): Promise<any> => {
        let prefixSelector = this.defaultSelector;

        if (strategy === 'UISelector') {
            prefixSelector = 'android=';
        }
        return $$(`${prefixSelector}${selector}`);
    }

    typeText = async(selector:string, text:string, strategy:string = 'CSS || XPATH'): Promise<any> => {
        const element = await this.finder(selector, strategy);
        await element.clearValue();
        await element.addValue(text);
    }

    getText = async(selector:string, strategy:string = 'CSS || XPATH'): Promise<string> => {
        const element = await this.finder(selector, strategy);
        const getText = await element.getText();
        return getText;
    }

    clickOn = async(selector:string, strategy:string = 'CSS || XPATH') => {
        console.log('ini selektor', typeof selector);
        const _selec:string = selector;
        const element = await this.finder(_selec, strategy);
        await element.click();
    }

     /**
     * for changing value that having text %var with dynamicValue
     * i.e  '//span[normalize-space()="%var"]';
     */
    builderElement = async(element:string, dynamicValue:string): Promise<string> => {
        this.selector = element.replace('%var', dynamicValue);
        return this.selector;
    }
}