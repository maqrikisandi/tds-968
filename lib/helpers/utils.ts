class Utils {
    /*
        This function to generate PhoneNumber without zero padding at first
    */
    fakerPhoneGenerator = async () => {
        const prefixProvider: string = '712';
        const timestampInSecond: number = Math.round(new Date().getTime() / 1000);
        return `${prefixProvider}${timestampInSecond}`;
    };

    /*
        This function to generate Nomor Induk Kependudukan using Dukcapil Valid Format
    */
    fakerNIKGenerator = async (prefix6Digit:string = '321503') => {
        const today: Date = new Date();
        const yy: string = today.getFullYear().toString().substring(2);
        const mm: number = today.getMonth() + 1; // Months start at 0!
        const dd: number = today.getDate();
        let _dd: string = '';
        let _mm: string = '';
        

        if (dd < 10) _dd = `0${dd}`;
        if (mm < 10) _mm = `0${mm}`;

        const dateOfBirth: string = _dd + _mm + yy;
        const runningNoNIK: number = Math.floor(Math.random() * 2001) + 1001;
        return `${prefix6Digit}${dateOfBirth}${runningNoNIK}`;
    };

    randomAlphabetPicker = async ():Promise<string> => {
        const alphabetArr: string[] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
        return alphabetArr[Math.floor(Math.random() * alphabetArr.length)];
    };

    /*
        This function to generate NomorPolisi randomize
    */
    fakerPoliceNoGenerator = async () => {
        let arrCharPoliceNo: string[] = [];
        // loop 3 times to collect char for construct policeNo
        for (let index = 0; index < 3; index += 1) {
            arrCharPoliceNo.push(await this.randomAlphabetPicker());
        }
        const prefixPoliceNo = arrCharPoliceNo[0];
        const suffixPoliceNo = `${arrCharPoliceNo[1]}${arrCharPoliceNo[2]}`;
        const runningNo = Math.floor(Math.random() * 8998) + 1001;
        return `${prefixPoliceNo}${runningNo}${suffixPoliceNo}`;
    };

    /*
        This function to normalize format Rp XXX.XXX.XXX into XXXXXXXXX
    */
    priceToNumber = (stringPriceRp:string) => {
        const normalizeText: string = stringPriceRp.replace(/[^0-9]/g, '');
        return normalizeText;
    };

    numberToPrice = (stringNumberRp:string) => {
        const normalizePrice: string = stringNumberRp.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        return normalizePrice;
    };

    sendReportPortalLog = async () => {
        // ReportingApi.log('INFO');
    };
}

export default new Utils();